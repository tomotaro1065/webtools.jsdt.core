# NJSDoc for JSDT #

This is fork of JSDT integrating [NJSDoc](https://bitbucket.org/nexj/njsdoc). 

# Status #

* Type resolution for other files is using NJSDoc as its data source 
* Content assist uses NJSDoc
* Hover Doc uses NJSDoc
* Jump to definition uses NJSDoc
* Screenshots [here](http://imgur.com/a/bT4Cd)

# Update site #

JSDT and NJSDoc are available on this update site: 

    https://bitbucket.org/nexj/updatesite/raw/default 

# Configuration #

The first step is to configure your .project file to enable JSDT and the NJSDoc builder

    <?xml version="1.0" encoding="UTF-8"?>
    <projectDescription>
    	<name>..</name>
    	<buildSpec>
    		<!-- For be performance remove org.eclipse.wst.jsdt.core.javascriptValidator -->
    		<!-- Add this -->
    		<buildCommand>
    			<name>org.eclipse.wst.jsdt.core.njsdocBuilder</name>
    			<arguments>
    			</arguments> 
    		</buildCommand>
    	</buildSpec>
    	<natures>
    		<nature>org.eclipse.wst.jsdt.core.jsNature</nature>
    	</natures>
    </projectDescription>

Next add configuration so that NJSDoc knows the relationship between the Javascript files in your project. NJSDoc uses this information to execute JavaScript 
in the appropriate order to build the data structures that drive content assist. Create a file under '.settings' folder called '.njsdoc'. It's contents are 
JavaScript. Each expression specifies an execution model, there can be multiple per project. Compatible with NJSDoc --recipe files. Restart Eclipse after changing 
recipe files. For example:

    // Execute files in a specific order specified by a sequence file
	//NJSDoc.defineSequence(<base path>, <list of paths, one per line>, [optional line filter regex]);
    NJSDoc.defineSequence("src/", "otherProjectName/src/../files.lst");
	NJSDoc.defineSequence("src/", "otherProjectName/src/../files.lst", "<script.*src=\"(.*)\\?");

	// Advanced arbitrary recipe with files, sequence files and evaluated code
	// file(), sequence(), and eval() can each be included zero or more times
    NJSDoc.recipe("mobile web")
	  .file("external/jquery.js") // project relative file path
	  .sequence("src/", "otherProjectName/src/../files.lst") // same as defineSequence() above
	  .sequence("src/", "otherProjectName/application.hta", "<script.*src=\"(.*)\\?") // same as defineSequence() above
	  .eval("var g = new MyGlobalObject()") // arbitrary expressions
	  .define(); // complete the definition

    // If execution order doesn't matter use modules()
    NJSDoc.recipe("amd based library")
      .file("src/sys.js")
      .modules({path: "src/", exclude: ["src/end.js", "src/sys.js"]})
      .file("end/sys.js");

# Build #

* Clone the standard JSDT repositories
* Clone this Mercurial repository: https://bitbucket.org/nexj/njsdoc-build and import this Eclipse project into your workspace
* For webtools.jsdt.core repository get the code from here (https://bitbucket.org/nexj/webtools.jsdt.core) and switch to the njsdoc branch

	  

