package org.eclipse.wst.jsdt.internal.njsdoc;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.StringWriter;

import com.nexj.njsdoc.DocumentedSlot;
import com.nexj.njsdoc.JSClass;
import com.nexj.njsdoc.ProblemRequestor;
import com.nexj.njsdoc.export.Function;
import com.nexj.njsdoc.export.HTMLDocumentationExporter;
import com.nexj.njsdoc.export.Variable;

/**
 * Utility methods for rendering labels for NJSDoc slots
 * 
 * @since 2.0
 */
public class NJSDocLabelFormatter {

	public enum Flavor {FULL, CONCISE, VERY_CONCISE}
	
	/**
	 * @param clazz
	 *            The class associated with the slot, may be null
	 * @param name
	 *            The slot name
	 * @param slot
	 *            The slot
	 * @param concise
	 *            If true then return type is not included in method and
	 *            function titles
	 * @return A string describing the parameters
	 */
	public static String getLabel(JSClass clazz, String name, DocumentedSlot slot, Flavor flav) {
		StringWriter sw = new StringWriter();
		BufferedWriter br = new BufferedWriter(sw);

		try {
			switch (slot.getValue().getType()) {
			case FIELD:
				if (clazz != null && flav == Flavor.FULL) {
					new HTMLDocumentationExporter(br).writeFieldTitle(clazz, new Variable(name, slot,
							ProblemRequestor.NULL_REQUESTOR));
					break;
				}
				//$FALL-THROUGH$
			case DATA:
				if (flav == Flavor.VERY_CONCISE) {
					br.append(new Variable(name, slot, ProblemRequestor.NULL_REQUESTOR).getFullName());
				} else {
					new HTMLDocumentationExporter(br)
							.writeVariableTitle(new Variable(name, slot, ProblemRequestor.NULL_REQUESTOR));
				}
				break;

			case CONSTRUCTOR:
				if (clazz != null) {
					new HTMLDocumentationExporter(br)
							.writeConstructorTitle(clazz);
					break;
				}
				//$FALL-THROUGH$
			case METHOD:
				if (clazz != null && flav == Flavor.FULL) {
					new HTMLDocumentationExporter(br).writeMethodTitle(clazz, new Function(name, slot,
							ProblemRequestor.NULL_REQUESTOR));
					break;
				}
				//$FALL-THROUGH$
			case FUNCTION:
				if (flav == Flavor.FULL) {
					new HTMLDocumentationExporter(br).writeFunctionTitle(new Function(name, slot,
							ProblemRequestor.NULL_REQUESTOR));
				} else {
					new HTMLDocumentationExporter(br).writeFunctionTitleNoRet(new Function(name, slot,
							ProblemRequestor.NULL_REQUESTOR));
				}
				break;
			default:
				throw new IllegalStateException();
			}
			br.flush();
		} catch (IOException e) {
			return null;
		}

		return sw.toString();
	}

	public static String getDetail(JSClass clazz, String name, DocumentedSlot slot) {
		StringWriter sw = new StringWriter();
		BufferedWriter br = new BufferedWriter(sw);

		try {
			switch (slot.getValue().getType()) {
			case FIELD:
				if (clazz != null) {
					new HTMLDocumentationExporter(br).writeFieldDetail(clazz, new Variable(name, slot,
							ProblemRequestor.NULL_REQUESTOR));
					break;
				}
				//$FALL-THROUGH$
			case DATA:
				new HTMLDocumentationExporter(br).writeVariableDetail(new Variable(name, slot,
						ProblemRequestor.NULL_REQUESTOR));
				break;

			case CONSTRUCTOR:
				if (clazz != null) {
					new HTMLDocumentationExporter(br).writeConstructorDetail(clazz);
					break;
				}
				//$FALL-THROUGH$
			case METHOD:
				if (clazz != null) {
					new HTMLDocumentationExporter(br).writeMethodDetail(clazz, new Function(name, slot,
							ProblemRequestor.NULL_REQUESTOR));
					break;
				}
				//$FALL-THROUGH$
			case FUNCTION:
				new HTMLDocumentationExporter(br).writeFunctionDetail(new Function(name, slot,
						ProblemRequestor.NULL_REQUESTOR));
				break;

			default:
				throw new IllegalStateException();
			}
			br.flush();
		} catch (IOException e) {
			return null;
		}

		return sw.toString();
	}
}
