package org.eclipse.wst.jsdt.internal.njsdoc.model;

import org.eclipse.core.runtime.CoreException;

/**
 * Execution model where execution order is not important. For example AMD modules. 
 * @since 2.0
 */
public class ModuleExecutionModel extends ExecutionModel {

    public ModuleExecutionModel(ProjectModel parent) throws CoreException {
        super(parent, null);
        addModulesStep("", null);
        makeStepsReadOnly();
    }

    /**
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "Non-sequential reconcile for project \"" + m_parent.getProject() + "\"";
    }
}
