/*******************************************************************************
 * Copyright (c) 2012 EclipseSource.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Ralf Sternberg - initial implementation and API
 ******************************************************************************/
package org.eclipse.wst.jsdt.internal.core.builder;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IResourceDelta;
import org.eclipse.core.resources.IResourceDeltaVisitor;
import org.eclipse.core.resources.IResourceVisitor;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.wst.jsdt.internal.njsdoc.model.ProjectModel;

class NJSDocBuilderVisitor implements IResourceVisitor, IResourceDeltaVisitor {

	private final ProjectModel project;
	private List<IFile> m_files = new ArrayList<IFile>();

	public NJSDocBuilderVisitor(ProjectModel project) throws CoreException {
		this.project = project;
	}

	public boolean visit(IResourceDelta delta) throws CoreException {
		return visit(delta.getResource());
	}

	public boolean visit(IResource resource) throws CoreException {
		if (resource.exists()) {
			if (resource.getType() != IResource.FILE) {
				return true;
			}

			IFile file = (IFile) resource;

			// TODO: use content type rather than extension
			if ("js".equals(file.getFileExtension())) {
				project.invalidateBytecode(file);
				m_files.add(file);
			}
		}
		return false;
	}

	public IFile[] getFiles() {
		return m_files.toArray(new IFile[m_files.size()]);
	}

}
