package org.eclipse.wst.jsdt.internal.njsdoc;


import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.wst.jsdt.internal.njsdoc.model.ExecutionModel;
import org.eclipse.wst.jsdt.internal.njsdoc.model.ProjectModel;

/**
 * @since 2.0
 * 
 */
public class NJSDocReconcileJob extends Job {

	protected final ProjectModel project;

	private final IFile[] files;

	public NJSDocReconcileJob(ProjectModel project, IFile[] files) {
		super("NJSDoc reconcile for " + project.getProject().getName());
		this.files = files;
		this.project = project;
	}

	/**
	 * @see org.eclipse.core.runtime.jobs.Job#run(org.eclipse.core.runtime.IProgressMonitor)
	 */
	@Override
	protected final IStatus run(IProgressMonitor monitor) {
		for (ExecutionModel seq : project.getExecutionModels()) {
			if (seq.executionNeeded() || contains(seq)) {
				seq.execute();
			}
		}
		return Status.OK_STATUS;
	}

	private boolean contains(ExecutionModel seq) {
		for (IFile file : files) {
			if (seq.contains(file)) {
				return true;
			}
		}
		return false;
	}

}
