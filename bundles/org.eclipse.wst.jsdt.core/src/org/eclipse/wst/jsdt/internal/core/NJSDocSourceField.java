package org.eclipse.wst.jsdt.internal.core;

import java.util.Collection;

import com.nexj.njsdoc.DocumentedSlot;
import com.nexj.njsdoc.org.mozilla.javascript.CodeLocation;

import org.eclipse.core.resources.IContainer;
import org.eclipse.wst.jsdt.internal.compiler.lookup.FieldBinding;
import org.eclipse.wst.jsdt.internal.compiler.lookup.NJSDocBinding;
import org.eclipse.wst.jsdt.internal.njsdoc.IDocumentedJavaScriptElement;
import org.eclipse.wst.jsdt.internal.njsdoc.NJSDocLabelFormatter;
import org.eclipse.wst.jsdt.internal.njsdoc.NJSDocLabelFormatter.Flavor;

/**
 * 
 */
public class NJSDocSourceField extends SourceField implements IDocumentedJavaScriptElement {

	private final boolean isStatic;

	public NJSDocSourceField(NJSDocSourceType parent, FieldBinding binding) {
		super(parent, new String(binding.name));
		isStatic = binding.isStatic();
	}

	/**
	 * @see org.eclipse.wst.jsdt.internal.core.JavaElement#getParent()
	 */
	public NJSDocSourceType getParent() {
		return (NJSDocSourceType) super.getParent();
	}

	public NJSDocBinding getParentBinding() {
		return getParent().getBinding();
	}

	private DocumentedSlot getSlot() {
		// See org.eclipse.wst.jsdt.internal.compiler.lookup.NJSDocBinding.makeMembers()
		// TODO: be more precise
		if (!isStatic) {
			DocumentedSlot slot = getParentBinding().getJSClass().findMember(name);

			if (slot != null) {
				return slot;
			}
		}

		return getParentBinding().getSlot().getValue().getSlot(name);
	}

	/**
	 * @see org.eclipse.wst.jsdt.internal.njsdoc.IDocumentedJavaScriptElement#getLabel()
	 */
	public String getLabel() {
		return NJSDocLabelFormatter.getLabel(getParentBinding().getJSClass(), name, getSlot(), Flavor.FULL);
	}

	/**
	 * @see org.eclipse.wst.jsdt.internal.njsdoc.IDocumentedJavaScriptElement#getDetailHTML()
	 */
	public String getDetailHTML() {
		return NJSDocLabelFormatter.getDetail(getParentBinding().getJSClass(), name, getSlot());
	}

	/**
	 * @see org.eclipse.wst.jsdt.internal.njsdoc.IDocumentedJavaScriptElement#getLocations()
	 */
	public Collection<CodeLocation> getLocations() {
		return getSlot().getLocations();
	}

	/**
	 * @see org.eclipse.wst.jsdt.internal.njsdoc.IDocumentedJavaScriptElement#getBaseResource()
	 */
	public IContainer getBaseResource() {
		return getParent().getBaseResource();
	}
}
