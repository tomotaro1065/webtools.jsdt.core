package org.eclipse.wst.jsdt.internal.njsdoc;

import java.util.Collection;

import com.nexj.njsdoc.org.mozilla.javascript.CodeLocation;

import org.eclipse.core.resources.IContainer;
import org.eclipse.wst.jsdt.core.IJavaScriptElement;

/**
 * @since 2.0
 */
public interface IDocumentedJavaScriptElement extends IJavaScriptElement {

	/**
	 * @return 1-line summary for this element.
	 */
	String getLabel();

	/**
	 * @return Full documentation for this element in HTML format.
	 */
	String getDetailHTML();

	/**
	 * @return Array of code locations.
	 */
	Collection<CodeLocation> getLocations();
	
	/**
	 * @return The resource that {@link #getLocations()} is relative to.
	 */
	IContainer getBaseResource();
}
