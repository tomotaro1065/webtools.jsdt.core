package org.eclipse.wst.jsdt.internal.core.builder;

import java.util.Map;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResourceDelta;
import org.eclipse.core.resources.IncrementalProjectBuilder;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.wst.jsdt.internal.njsdoc.NJSDocReconcileJob;
import org.eclipse.wst.jsdt.internal.njsdoc.model.NJSDocModel;
import org.eclipse.wst.jsdt.internal.njsdoc.model.ProjectModel;

/**
 * 
 */
public class NJSDocBuilder extends IncrementalProjectBuilder {

	public NJSDocBuilder() {
	}

	/**
	 * @see org.eclipse.core.resources.IncrementalProjectBuilder#build(int,
	 *      java.util.Map, org.eclipse.core.runtime.IProgressMonitor)
	 */
	protected IProject[] build(int kind, Map<String, String> args, IProgressMonitor monitor)
			throws CoreException {
		ProjectModel projectModel = getProjectModel();

		if (projectModel == null) {
		    return null; // Configuration error
		}

		NJSDocBuilderVisitor visitor = new NJSDocBuilderVisitor(projectModel);
		if (kind == IncrementalProjectBuilder.FULL_BUILD) {
			getProject().accept(visitor);
		} else {
			IResourceDelta delta = getDelta(getProject());
			if (delta == null) {
				getProject().accept(visitor);
			} else {
				delta.accept(visitor);
			}
		}

		new NJSDocReconcileJob(projectModel, visitor.getFiles()).schedule();
		return null;
	}

	private ProjectModel getProjectModel() {
		return NJSDocModel.getModel().getProject(getProject());
	}
}
