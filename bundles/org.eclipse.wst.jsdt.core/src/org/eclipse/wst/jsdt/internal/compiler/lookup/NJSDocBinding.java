package org.eclipse.wst.jsdt.internal.compiler.lookup;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.nexj.njsdoc.DocumentedSlot;
import com.nexj.njsdoc.JSClass;
import com.nexj.njsdoc.ProblemRequestor;
import com.nexj.njsdoc.JSClass.SlotIterator;
import com.nexj.njsdoc.SlotValue;
import com.nexj.njsdoc.export.DocumentationExporter.Argument;
import com.nexj.njsdoc.export.Function;
import com.nexj.njsdoc.export.OutputCreator;
import com.nexj.njsdoc.export.Variable;

import org.eclipse.core.resources.IContainer;
import org.eclipse.wst.jsdt.core.compiler.CharOperation;
import org.eclipse.wst.jsdt.internal.compiler.classfmt.ClassFileConstants;
import org.eclipse.wst.jsdt.internal.njsdoc.model.ExecutionModel;

/**
 * 
 */
public class NJSDocBinding extends ReferenceBinding {

	private final ExecutionModel model;

	private final DocumentedSlot slot;

	/**
	 * May be null
	 */
	private final JSClass clazz;

	private final boolean isVariable;

	private MethodBinding[] methods;
	private FieldBinding[] fields;

	/**
	 * Constructor for normal values
	 * 
	 * @param slot The slot
	 * @param model The owning execution model
	 */
	private NJSDocBinding(DocumentedSlot slot, JSClass clazz, boolean isVariable, ExecutionModel model) {
		this.slot = slot;
		this.model = model;
		this.isVariable = isVariable;
		this.clazz = clazz;

		String name = slot.getName();

		if (name == null) {
			name = "<Global>";
		}

		this.sourceName = name.toCharArray();
		this.compoundName = new char[1][];
		this.compoundName[0] = sourceName;

		this.fileName = slot.getFiles().toString().toCharArray();

		this.modifiers = ClassFileConstants.AccPublic;
		computeId();
	}

	public static NJSDocBinding makeGlobal(SlotValue globalValue, ExecutionModel model) {
		return make(DocumentedSlot.makeGlobal(globalValue), model);
	}

	public static NJSDocBinding make(DocumentedSlot slot, ExecutionModel model) {
		// Future: Should not be necessary
		SlotValue value = slot.getValue();
		boolean isVariable = value.getInstanceJSClass() != null && value.getConstructorJSClass() == null;
		JSClass clazz;

		if (isVariable) {
			clazz = value.getInstanceJSClass();
		} else {
			clazz = value.getConstructorJSClass();
		}

		// TODO: why is slot not clazz.getSlot()?
		return new NJSDocBinding(slot, clazz, isVariable, model);
	}

	public static NJSDocBinding make(JSClass clazz, ExecutionModel model) {
		return new NJSDocBinding(clazz.getSlot() /*?*/, clazz, false, model);
	}

	public DocumentedSlot getSlot() {
		return slot;
	}

	/**
	 * @return May be null
	 */
	public JSClass getJSClass() {
		return clazz;
	}
	
	public boolean isVariable() {
		return isVariable;
	}

	/**
	 * @see OutputCreator
	 */
	private void makeMembers() {
		if (methods == null) {
			List<MethodBinding> methods = new ArrayList<MethodBinding>();
			List<FieldBinding> fields = new ArrayList<FieldBinding>();

			// TODO: should this block be removed? 
			if (clazz != null) {
				// Methods
				for (SlotIterator it = clazz.getMethodIterator(); it.hasNext();) {
					it.next();
					methods.add(makeFunctionMember(it.getSlotName(), it.getSlotValue(), false));
				}

				// Fields
				for (SlotIterator it = clazz.getFieldIterator(); it.hasNext();) {
					it.next();
					fields.add(makeFieldMember(it.getSlotName(), it.getSlotValue(), false));
				}
			}

			// TODO: should show either static or non-static, not both

			// Non static methods and fields
			if (slot != null) {
				SlotValue doc = slot.getValue();

				for (Iterator<String> iterator = doc.slotNameIterator(); iterator.hasNext();) {
					String childSlotName = iterator.next();
					DocumentedSlot childSlot = doc.getSlot(childSlotName);
					SlotValue childValue = childSlot.getValue();
	
					if ("prototype".equals(childSlotName) || SlotValue.OBJECT_PROTO.equals(childSlotName)) {
						continue;
					}
	
					switch (childValue.getType()) {
					case FIELD:
						fields.add(makeFieldMember(childSlotName, childSlot, doc.isNamespace()));
						break;
					case DATA:
						fields.add(makeFieldMember(childSlotName, childSlot, doc.isNamespace()));
						break;
					case METHOD:
						methods.add(makeFunctionMember(childSlotName, childSlot, doc.isNamespace()));
						break;
					case FUNCTION:
						methods.add(makeFunctionMember(childSlotName, childSlot, doc.isNamespace()));
						break;
	
					case CONSTRUCTOR:
						// TODO: something else?
						methods.add(makeFunctionMember(childSlotName, childSlot, doc.isNamespace()));
						break;
					}
				}
			}

			// Initialize internal fields
			this.methods = methods.toArray(new MethodBinding[methods.size()]);
			this.fields = fields.toArray(new FieldBinding[fields.size()]);
		}
	}

	private FieldBinding makeFieldMember(String slotName, DocumentedSlot slotValue, boolean bStatic) {
		Variable var = new Variable(slotName, slotValue, ProblemRequestor.NULL_REQUESTOR);
		int modifiers = ClassFileConstants.AccDefault;

		if (bStatic) {
			modifiers |= ClassFileConstants.AccStatic;
		}

		return new FieldBinding(slotName.toCharArray(), resolveType(var.getType()), modifiers, this);
	}

	private MethodBinding makeFunctionMember(String slotName, DocumentedSlot childSlot, boolean bStatic) {
		List<TypeBinding> parameters = new ArrayList<TypeBinding>();
		Function fun = new Function(slotName, childSlot, ProblemRequestor.NULL_REQUESTOR);

		for (Iterator<Argument> argIt = fun.getArgumentIterator(); argIt.hasNext();) {
			Argument arg = argIt.next();

			parameters.add(resolveType(arg.getType()));
		}

		int modifiers = ClassFileConstants.AccPublic;
		
		if (bStatic) {
			modifiers |= ClassFileConstants.AccStatic;
		}

		return new MethodBinding(modifiers, slotName.toCharArray(), resolveType(fun.getReturnType()),
				parameters.toArray(new TypeBinding[parameters.size()]), this);
	}

	private TypeBinding resolveType(String typeName) {
		// TODO Auto-generated method stub
		return TypeBinding.ANY;
	}

	/**
	 * @see org.eclipse.wst.jsdt.internal.compiler.lookup.ReferenceBinding#getMethods(char[])
	 */
	public MethodBinding[] getMethods(char[] selector) {
		makeMembers();

		for (int i = 0; i < methods.length; i++) {
			if (CharOperation.equals(selector, methods[i].selector)) {
				return new MethodBinding[] { methods[i] };
			}
		}

		return super.getMethods(selector);
	}

	/**
	 * @see org.eclipse.wst.jsdt.internal.compiler.lookup.ReferenceBinding#methods()
	 */
	public MethodBinding[] methods() {
		makeMembers();

		return methods;
	}

	/**
	 * @see org.eclipse.wst.jsdt.internal.compiler.lookup.ReferenceBinding#getField(char[],
	 *      boolean)
	 */
	public FieldBinding getField(char[] selector, boolean needResolve) {
		makeMembers();

		for (int i = 0; i < fields.length; i++) {
			if (CharOperation.equals(selector, fields[i].name)) {
				return fields[i];
			}
		}

		return super.getField(selector, needResolve);
	}

	/**
	 * @see org.eclipse.wst.jsdt.internal.compiler.lookup.ReferenceBinding#fields()
	 */
	public FieldBinding[] fields() {
		makeMembers();

		return fields;
	}

	public IContainer getBaseResource() {
		return model.getParent().getProject();
	}
	
	/**
	 * @see org.eclipse.wst.jsdt.internal.compiler.lookup.ReferenceBinding#superclass()
	 */
	public ReferenceBinding superclass() {
		if (clazz != null) {
			JSClass base = clazz.getBase();
	
			if (base != null) {
			    return new NJSDocBinding(base.getSlot(), base, isVariable, model);
			}
		}

		return null;
	}
}

