package org.eclipse.wst.jsdt.internal.core;

import java.util.Collection;

import com.nexj.njsdoc.DocumentedSlot;
import com.nexj.njsdoc.org.mozilla.javascript.CodeLocation;

import org.eclipse.core.resources.IContainer;
import org.eclipse.wst.jsdt.internal.compiler.lookup.MethodBinding;
import org.eclipse.wst.jsdt.internal.compiler.lookup.NJSDocBinding;
import org.eclipse.wst.jsdt.internal.njsdoc.IDocumentedJavaScriptElement;
import org.eclipse.wst.jsdt.internal.njsdoc.NJSDocLabelFormatter;
import org.eclipse.wst.jsdt.internal.njsdoc.NJSDocLabelFormatter.Flavor;

/**
 * 
 */
public class NJSDocSourceMethod extends SourceMethod implements IDocumentedJavaScriptElement {

	private final boolean isStatic;

	public NJSDocSourceMethod(NJSDocSourceType parent, MethodBinding binding) {
		super(parent, new String(binding.selector), makeArgTypes(binding));
		isStatic = binding.isStatic();
	}

	private static String[] makeArgTypes(MethodBinding binding) {
		String[] argTypes = new String[binding.parameters.length];

		for (int i = 0; i < argTypes.length; i++) {
			argTypes[i] = new String(binding.parameters[i].computeUniqueKey());
		}

		return argTypes;
	}

	/**
	 * @see org.eclipse.wst.jsdt.internal.core.JavaElement#getParent()
	 */
	public NJSDocSourceType getParent() {
		return (NJSDocSourceType) super.getParent();
	}
	
	public NJSDocBinding getParentBinding() {
		return getParent().getBinding();
	}

	private DocumentedSlot getSlot() {
		// See org.eclipse.wst.jsdt.internal.compiler.lookup.NJSDocBinding.makeMembers()
		// TODO: be more precise
		if (!isStatic) {
			DocumentedSlot slot = getParentBinding().getJSClass().findMember(name);

			if (slot != null) {
				return slot;
			}
		}

		return getParentBinding().getSlot().getValue().getSlot(name);
	}
	
	/**
	 * @see org.eclipse.wst.jsdt.internal.njsdoc.IDocumentedJavaScriptElement#getLabel()
	 */
	public String getLabel() {
		return NJSDocLabelFormatter.getLabel(getParentBinding().getJSClass(), name, getSlot(), Flavor.FULL);
	}

	/**
	 * @see org.eclipse.wst.jsdt.internal.njsdoc.IDocumentedJavaScriptElement#getDetailHTML()
	 */
	public String getDetailHTML() {
		return NJSDocLabelFormatter.getDetail(getParentBinding().getJSClass(), name, getSlot());
    }

	/**
	 * @see org.eclipse.wst.jsdt.internal.njsdoc.IDocumentedJavaScriptElement#getBaseResource()
	 */
	public IContainer getBaseResource() {
		return getParent().getBaseResource();
	}

	/**
	 * @see org.eclipse.wst.jsdt.internal.njsdoc.IDocumentedJavaScriptElement#getLocations()
	 */
	public Collection<CodeLocation> getLocations() {
		return getSlot().getLocations();
	}
}
